#Functionality:
This application is simple application that saves user data into database and presents info back to them. User session id 
is held inside "token" cookie. Server side validation errors are visible inside console log.

#Prerequisites 
- add a cookie with a key "token" and random value to be able to save results

#Notes
- I decided to make the application as simple as possible.
- Decided to use cookies due to them being sent by browser automatically. Session tokens would have been same only the frontend app
should have needed to send it with ajax request. In real application there should have been used authentication. For example
Spring Security. 
- For database, I chose h2 because it is easy to set up for exercises. Also, data.sql file shows fully the database structure.
If this were production exercise, then we shouldn't have used in memory database. Database choice comes from company preferences
and depending on what the project needs. Non sql database would have been the easiest to handle nested sectors (for example Mongo),
but database choice does not change java implementation drastically due to JPA doing the heavy lifting.
- Chose template language (Thymeleaf) over SPA (React) due to frontend being mostly static. If website was dynamic then
SPA framework/library would have been better fit. The logic would have stayed the same but instead of handling everything 
with models it would have been json requests. As a side note template language is better for SEO because it is server side rendered. 
