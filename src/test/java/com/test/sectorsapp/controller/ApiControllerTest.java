package com.test.sectorsapp.controller;

import com.test.sectorsapp.controller.dtos.ApiRequest;
import com.test.sectorsapp.logic.api.ApiService;
import com.test.sectorsapp.logic.sectors.SectorsService;
import jakarta.servlet.http.Cookie;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.WebMvcTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.ResultMatcher;

import java.util.Arrays;
import java.util.List;

import static org.mockito.Mockito.*;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post;
import static org.springframework.test.web.servlet.result.MockMvcResultHandlers.print;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

@WebMvcTest(ApiController.class)
public class ApiControllerTest {

	@Autowired
	private MockMvc mockMvc;

	@MockBean
	private ApiService service;

	@MockBean
	private SectorsService sectorsService;

	@BeforeEach
	private void init() {
		when(sectorsService.isSectorPresent(anyString())).thenReturn(false);
		when(sectorsService.isSectorPresent(eq("1"))).thenReturn(true);
		when(sectorsService.isSectorPresent(eq("101"))).thenReturn(true);
	}

	@Test
	public void testValidationNameMissing() throws Exception {
		testFail("""
				 {
				 "sectors":["1"],
				 "terms": true
				}
				""");
	}

	@Test
	public void testValidationNameWrongRegex() throws Exception {
		testFail("""
				 {
				 "name": "SELECT * FROM Users",
				 "sectors":["1"],
				 "terms": true
				}
				""");
	}

	@Test
	public void testValidationSectorsMissing() throws Exception {
		testFail("""
				 {
				 "name": "John Wick",
				 "terms": true
				}
				""");
	}

	@Test
	public void testValidationSectorsNull() throws Exception {
		testFail("""
				 {
				 "name": "John Wick",
				 "sectors":[null],
				 "terms": true
				}
				""");
	}

	@Test
	public void testValidationSectorsEmptyString() throws Exception {
		testFail("""
				 {
				 "name": "John Wick",
				 "sectors":[""],
				 "terms": true
				}
				""");
	}

	@Test
	public void testValidationSectorsEmptyList() throws Exception {
		testFail("""
				 {
				 "name": "John Wick",
				 "sectors":[],
				 "terms": true
				}
				""");
	}

	@Test
	public void testValidationSectorsNumberTooBig() throws Exception {
		testFail("""
				 {
				 "name": "John Wick",
				 "sectors":["100000000000000000"],
				 "terms": true
				}
				""");
	}

	@Test
	public void testValidationTermsMissing() throws Exception {
		testFail("""
				 {
				 "name": "John Wick",
				 "sectors":["1"]
				}
				""");
	}

	@Test
	public void testValidationTermsNull() throws Exception {
		testFail("""
				 {
				 "name": "John Wick",
				 "sectors":["1"],
				 "terms": null
				}
				""");

	}

	@Test
	public void testValidationTermsString() throws Exception {
		testFail("""
				 {
				 "name": "John Wick",
				 "sectors":["1"],
				 "terms": "null"
				}
				""");
	}

	@Test
	public void testValidationTermsFalse() throws Exception {
		testFail("""
				 {
				 "name": "John Wick",
				 "sectors":["1"],
				 "terms": false
				}
				""");
	}

	@Test
	public void testSuccessRequest() throws Exception {
		testSuccess("""
				 {
				 "name": "John Wick",
				 "sectors":["1", "101"],
				 "terms": true
				}
				""", "John Wick", Arrays.asList("1", "101"), true);
	}

	@Test
	public void testCookieWrong() throws Exception {
		test("""
				 {
				 "name": "John Wick",
				 "sectors":["1"],
				 "terms": true
				}
				""", status().is4xxClientError(), new Cookie("notToken", "test"));
	}

	private void testSuccess(String json, String name, List<String> sectors, boolean terms) throws Exception {
		test(json, status().isOk(), new Cookie("token", "test"));
		verify(service, atLeastOnce()).saveResult(
				eq("test"),
				eq(new ApiRequest(name, sectors, terms)));
	}

	private void testFail(String json) throws Exception {
		test(json, status().is4xxClientError(), new Cookie("token", "test"));
	}

	private void test(String json, ResultMatcher matcher, Cookie cookie) throws Exception {
		this.mockMvc.perform(
						post("/api/save")
								.contentType("application/json")
								.cookie(cookie)
								.content(json))
				.andDo(print())
				.andExpect(matcher);
	}
}
