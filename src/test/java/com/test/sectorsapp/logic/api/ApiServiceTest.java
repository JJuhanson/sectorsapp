package com.test.sectorsapp.logic.api;

import com.test.sectorsapp.controller.dtos.ApiRequest;
import com.test.sectorsapp.database.results.Result;
import com.test.sectorsapp.database.results.ResultsRepository;
import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.Test;
import org.mockito.Mockito;

import java.util.List;

import static org.mockito.ArgumentMatchers.eq;
import static org.mockito.Mockito.*;

public class ApiServiceTest {

	private static ResultsRepository mock;
	private static ApiService service;

	@BeforeAll
	public static void init() {
		mock = Mockito.mock(ResultsRepository.class);
		when(mock.findByToken("token1")).thenReturn(null);
		when(mock.findByToken("token2")).thenReturn(new Result(1,"token2", "Jack Black", List.of("101", "201"), true));
		service = new ApiService(mock);
	}

	@Test
	public void testNonExisting(){
		service.saveResult("token1", new ApiRequest("Ann Green", List.of("1"), true));
		verify(mock).save(eq(new Result(1, "token1", "Ann Green", List.of("1"), true)));
	}
	@Test
	public void testExisting(){
		service.saveResult("token2", new ApiRequest("Ann Green", List.of("1", "2"), true));
		verify(mock).save(eq(new Result(1, "token2", "Ann Green", List.of("1", "2"), true)));

	}
}
