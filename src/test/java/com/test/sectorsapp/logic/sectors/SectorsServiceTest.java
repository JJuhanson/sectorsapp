package com.test.sectorsapp.logic.sectors;

import com.test.sectorsapp.database.sectors.Sector;
import com.test.sectorsapp.database.sectors.SectorsRepository;
import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.Test;
import org.mockito.Mockito;

import java.util.Arrays;
import java.util.List;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertNull;
import static org.mockito.Mockito.when;

public class SectorsServiceTest {

	private static SectorsService service;

	private static final List<Sector> testingSectors = Arrays.asList(
			new Sector(1, "Parent", null),
			new Sector(2, "Another parent", null),
			new Sector(11, "Child 1", 1L),
			new Sector(12, "Child 2", 1L),
			new Sector(121, "Grandchild 1", 12L)

	);

	@BeforeAll
	public static void init() {
		SectorsRepository mock = Mockito.mock(SectorsRepository.class);
		when(mock.findAll()).thenReturn(testingSectors);
		service = new SectorsService(mock);
	}

	@Test
	public void test() {
		List<SectorWrapper> result = service.getSectors();

		assertEquals("Parent", result.get(0).getTitle());
		assertEquals("Another parent", result.get(1).getTitle());

		assertNull(result.get(0).getParentId());
		assertNull(result.get(1).getParentId());

		assertEquals(2, result.get(0).getSubSectors().size());
		assertEquals(0, result.get(1).getSubSectors().size());

		assertEquals("Child 1", result.get(0).getSubSectors().get(0).getTitle());
		assertEquals("Child 2", result.get(0).getSubSectors().get(1).getTitle());

		assertEquals(1L, result.get(0).getSubSectors().get(0).getParentId());
		assertEquals(1L, result.get(0).getSubSectors().get(1).getParentId());

		assertEquals(0, result.get(0).getSubSectors().get(0).getSubSectors().size());
		assertEquals(1, result.get(0).getSubSectors().get(1).getSubSectors().size());

		assertEquals("Grandchild 1", result.get(0).getSubSectors().get(1).getSubSectors().get(0).getTitle());
		assertEquals(12L, result.get(0).getSubSectors().get(1).getSubSectors().get(0).getParentId());
		assertEquals(0, result.get(0).getSubSectors().get(1).getSubSectors().get(0).getSubSectors().size());

	}

}
