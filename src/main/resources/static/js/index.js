$("#submit").on("click", function(){
    var formData = {
          name: $("#name").val(),
          sectors: $("#sectors").val(),
          terms: $("#terms").is(":checked")
        };

    $.ajax({
      type: "POST",
      url: "/api/save",
      data: JSON.stringify(formData),
      contentType: "application/json",
      encode: true,
    }).done(function(data){
        $("form").load("/form")
  } );
})