package com.test.sectorsapp.logic.api;

import com.test.sectorsapp.controller.dtos.ApiRequest;
import com.test.sectorsapp.database.results.Result;
import com.test.sectorsapp.database.results.ResultsRepository;
import org.springframework.stereotype.Service;

@Service
public class ApiService {

	private final ResultsRepository repository;

	public ApiService(ResultsRepository repository) {
		this.repository = repository;
	}

	public void saveResult(String token, ApiRequest dto) {
		Result result = getCurrentResult(token);
		if (result == null) {
			result = new Result(token);
		}
		result.setName(dto.getName());
		result.setSectors(dto.getSectors());
		result.setTermsAgreement(dto.getTerms());

		repository.save(result);
	}

	public Result getCurrentResult(String token) {
		if (token == null || token.trim().length() == 0) {
			return null;
		}
		return repository.findByToken(token);
	}
}
