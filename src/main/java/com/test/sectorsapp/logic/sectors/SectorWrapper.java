package com.test.sectorsapp.logic.sectors;

import com.test.sectorsapp.database.sectors.Sector;

import java.util.ArrayList;
import java.util.List;

public class SectorWrapper {
	private Sector sector;
	private SectorWrapper parent;
	private List<SectorWrapper> subSectors = new ArrayList<>();

	public SectorWrapper(Sector sector) {
		this.sector = sector;
	}

	public Long getId() {
		return sector.getId();
	}

	public String getTitle() {
		return sector.getTitle();
	}

	public Long getParentId() {
		return sector.getParentId();
	}

	public List<SectorWrapper> getSubSectors() {
		return subSectors;
	}

	public Integer getPadding() {
		if (isRootSector()) {
			return 0;
		}
		return parent.getPadding() + 10;
	}

	public boolean isRootSector() {
		return sector.getParentId() == null;
	}

	public void addAsChild(SectorWrapper child) {
		child.setParent(this);
		subSectors.add(child);
	}

	private void setParent(SectorWrapper sectorWrapper) {
		parent = sectorWrapper;
	}

}
