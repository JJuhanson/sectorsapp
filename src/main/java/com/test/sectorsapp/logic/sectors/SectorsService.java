package com.test.sectorsapp.logic.sectors;

import com.test.sectorsapp.database.sectors.SectorsRepository;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

@Service
public class SectorsService {

	private SectorsRepository repository;

	public SectorsService(SectorsRepository repository) {
		this.repository = repository;
	}

	public List<SectorWrapper> getSectors() {
		List<SectorWrapper> result = new ArrayList<>();
		Map<Long, SectorWrapper> map = repository.findAll()
				.stream()
				.map(SectorWrapper::new)
				.collect(Collectors.toMap(SectorWrapper::getId, wrapper -> wrapper, (x, y) -> y, LinkedHashMap::new));

		map.forEach((id, wrapper) -> {
			if (wrapper.isRootSector()) {
				result.add(wrapper);
				return;
			}
			if (map.containsKey(wrapper.getParentId())) {
				map.get(wrapper.getParentId()).addAsChild(wrapper);
			}
		});

		return result;
	}

	public Boolean isSectorPresent(String sector) {
		if (sector == null || sector.trim().length() == 0) {
			return false;
		}
		try {
			return repository.existsById(Long.parseLong(sector));
		} catch (NumberFormatException e) {
			return false;
		}
	}

}
