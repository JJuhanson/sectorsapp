package com.test.sectorsapp.database.results;

import org.springframework.data.jpa.repository.JpaRepository;

public interface ResultsRepository extends JpaRepository<Result, Long> {

	Result findByToken(String token);
}
