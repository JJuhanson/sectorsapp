package com.test.sectorsapp.database.results;

import jakarta.persistence.*;
import lombok.*;

import java.util.List;

@Entity
@Table(name = "RESULT")
@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
@EqualsAndHashCode
public class Result {

	@Id
	@Column(name = "ID")
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@EqualsAndHashCode.Exclude
	private long id;

	@Column(name = "TOKEN", nullable = false)
	private String token;

	@Column(name = "NAME")
	private String name;

	@Column(name = "SECTORS")
	private List<String> sectors;

	@Column(name = "TERMS_AGR")
	private Boolean termsAgreement;

	public Result(String token){
		this.token = token;
	}
}
