package com.test.sectorsapp.database.sectors;

import org.springframework.data.jpa.repository.JpaRepository;

public interface SectorsRepository extends JpaRepository<Sector, Long> {
}
