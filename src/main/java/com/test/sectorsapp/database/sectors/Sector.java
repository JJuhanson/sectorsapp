package com.test.sectorsapp.database.sectors;

import jakarta.persistence.*;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;

@Entity
@Table(name = "SECTOR")
@Getter
@AllArgsConstructor
@NoArgsConstructor
public class Sector {

	@Id
	@Column(name = "ID")
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private long id;

	@Column(name = "TITLE")
	private String title;

	@Column(name = "PARENT_ID")
	private Long parentId;
}
