package com.test.sectorsapp.controller;

import com.test.sectorsapp.database.results.Result;
import com.test.sectorsapp.logic.api.ApiService;
import com.test.sectorsapp.logic.sectors.SectorsService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.CookieValue;
import org.springframework.web.bind.annotation.GetMapping;

@Controller
public class PageController {

	@Autowired
	private SectorsService sectorsService;

	@Autowired
	private ApiService apiService;

	@GetMapping("/")
	public String index(Model model, @CookieValue(value = "token", required = false) String token) {
		fillModel(model, token);
		return "index";
	}

	@GetMapping("/form")
	public String form(Model model, @CookieValue("token") String token) {
		fillModel(model, token);
		return "components :: form";
	}

	private void fillModel(Model model, String token) {
		model.addAttribute("sectors", sectorsService.getSectors());
		Result result = apiService.getCurrentResult(token);
		model.addAttribute("userData", result == null ? new Result() : result);
	}

}
