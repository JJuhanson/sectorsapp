package com.test.sectorsapp.controller.dtos;

import jakarta.validation.constraints.NotNull;
import jakarta.validation.constraints.Pattern;
import lombok.AllArgsConstructor;
import lombok.EqualsAndHashCode;
import lombok.Getter;

import java.util.List;

@AllArgsConstructor
@EqualsAndHashCode
@Getter
public class ApiRequest {

	@Pattern(regexp = "[A-Za-z ,.'-]+", message = "Illegal characters used")
	@NotNull(message = "Name is mandatory")
	private String name;

	@NotNull(message = "Sectors is mandatory")
	private List<String> sectors;

	@NotNull(message = "Agree to terms is mandatory")
	private Boolean terms;
}
