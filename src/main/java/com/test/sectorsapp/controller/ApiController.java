package com.test.sectorsapp.controller;

import com.test.sectorsapp.controller.dtos.ApiRequest;
import com.test.sectorsapp.logic.api.ApiService;
import com.test.sectorsapp.logic.sectors.SectorsService;
import jakarta.validation.Valid;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.CookieValue;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;

import java.util.Collections;

@RestController
public class ApiController {

	@Autowired
	private ApiService service;

	@Autowired
	private SectorsService sectorsService;

	@PostMapping(value = "/api/save", consumes = "application/json", produces = "application/json")
	public ResponseEntity<?> save(@CookieValue("token") String token, @RequestBody @Valid ApiRequest dto) {
		if (isItNotValidRequest(dto)) {
			return new ResponseEntity<>(HttpStatus.BAD_REQUEST);
		}
		service.saveResult(token, dto);
		return ResponseEntity.ok(Collections.singletonMap("success", true));
	}

	private boolean isItNotValidRequest(ApiRequest dto) {
		return dto.getSectors().isEmpty()
				|| !dto.getSectors().stream().allMatch(sectorsService::isSectorPresent)
				|| !dto.getTerms();
	}

}
