package com.test.sectorsapp;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class SectorsAppApplication {

	public static void main(String[] args) {
		SpringApplication.run(SectorsAppApplication.class, args);
	}

}
